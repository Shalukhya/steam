<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="favicon.ico">

    <title>Steam Pay</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/carousel.css" rel="stylesheet">
</head>
<!-- NAVBAR
================================================== -->
<body>
<div class="navbar-wrapper">
    <div class="container">

        <div class="navbar navbar-inverse navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Steam</a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="#">Home</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="index.html">Currency Conversion</a></li>
                                <li><a href="productCat.php">Product Catalogue</a></li>
                                <li class="divider"></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
</div>
<!-- Carousel
================================================== -->
<div id="myCarousel" class="carousel slide">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
        <div class="item active">
            <img src="./images/1.jpg" alt="First slide">

        </div>
        <div class="item">
            <img src="./images/2.jpg" alt="Second slide">
        </div>
        <div class="item">
            <img src="./images/3.jpg" alt="Third slide">
        </div>
    </div>
    <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
</div><!-- /.carousel -->

<div class="container">

    <?php

    if(isset($_POST['product'])){

        $name = $_POST['msisdn'];
        $code=$_POST['product'];
        $amount=$_POST['curr'];
        $desc='';
        $amnt='';
        //Reading from file to get selected product record details
        $xml = file_get_contents("data/".date("Y-m-d").".xml");

        $xml1 = simplexml_load_string($xml);
        $json = json_encode($xml1);
        $array = json_decode($json,TRUE);

        for ($x=0; $x<count($array['items']['item']);$x++) {
           $devices= ($array['items']['item'][$x]);
           if(strcmp($devices["productCode"],$code)==0){
              $desc=$devices["productDesc"];
               $amount=$devices["amount"];
           }
        }
        if(preg_match("/^[94]{2}[76,77]{2}[0-9]{7}$/", $name)){
            echo '<div class="alert alert-success"><strong>Success!</strong>valid Number</div>';
            $apiCallXML = new SimpleXMLElement("<order></order>");
            $apiBody = $apiCallXML->addChild('correlator', '12345');
            $apiBody = $apiCallXML->addChild('publisherCode', 'ECLUB');
            $apiBody = $apiCallXML->addChild('productCode', $code);
            $apiBody = $apiCallXML->addChild('productName', 'Steam Voucher');
            $apiBody = $apiCallXML->addChild('productDesc', 'In game currency');
            $apiBody = $apiCallXML->addChild('amount', $amnt);
            $apiBody = $apiCallXML->addChild('currency', 'LKR');
            $apiBody = $apiCallXML->addChild('msisdn', $name);
            $apiBody = $apiCallXML->addChild('opco', 'DIALOG');
            Header('Content-type: text/xml');
            var_dump($apiCallXML->asXML());
            //make api call here
            $ch = curl_init("http://52.74.144.138/vds/order.php");
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/xml',
                'Accept: application/xml',
                'Authorization: bf0c2830189bd4d52b9b42b352b29e1b',
                'Accept-Charset: UTF-8'
            ));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS,$apiCallXML->asXML());
            $res = curl_exec($ch);
            var_dump($res);
            curl_close($ch);

        } else{
            echo 	 '<div class="alert alert-danger">
		 <strong>Danger!</strong> Input valid Number
		</div>';
        }

    }
    ?>
    <div class="panel-body">
        <form method='POST'>
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th><a  href="#">Id</a> <i class="fa fa-sort"></i></th>
                    <th><a  href="#">Code</a> <i class="fa fa-sort"></i></th>
                    <th><a  href="#">Name</a> <i class="fa fa-sort"></i></th>
                    <th><a  href="#">Description</a> <i class="fa fa-sort"></i></th>
                    <th><a  href="#">Opco</a> <i class="fa fa-sort"></i></th>
                    <th><a  href="#">Amount</a> <i class="fa fa-sort"></i></th>
                    <th><a  href="#">Currency</a> <i class="fa fa-sort"></i></th>
                    <th><a  href="#">Status</a> <i class="fa fa-sort"></i></th>
                    <th colspan="2">Action</th>
                </tr>
                </thead>


                <?php

                function displayContents(){
                    $devices = array();

                    $xml = file_get_contents("data/".date("Y-m-d").".xml");

                    $xml1 = simplexml_load_string($xml);
                    $json = json_encode($xml1);
                    $array = json_decode($json,TRUE);
                    // echo $json;
                    //var_dump($array);


                    for ($x=0; $x<count($array['items']['item']);$x++) {
                        $devices= ($array['items']['item'][$x]);
                        echo " <tr>
                        <td><input type='radio' value='" . $devices["productCode"]."' name='product'></td>
                        <td>" . $devices["publisherCode"]. "</td>
                        <td>" . $devices["opco"] . "</td>
                        <td>" . $devices["productCode"] . "</td>
                        <td>" . $devices["productName"] . "</td>
                        <td>" . $devices["productDesc"] . "</td>
                        <td>" . $devices["amount"] . "</td>
                        <td>" . $devices["currency"] . "</td>
                        <td><span class='text-success'>active</span></td>
                        <td><a href='#'>Edit</a></td>
                    </tr>";
                    }
                }

                if (file_exists("data/".date("Y-m-d").".xml")) {
                    displayContents();
                } else {
                    require ('product.php');;
                    displayContents();
                }
                ?>

            </table>
            <input type="text" name="msisdn" placeholder="phone number">
            <input type ="submit" value="Buy It"> </input>
        </form>
    </div>


</div>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="./js/jquery.js"></script>
<script src="./js/bootstrap.min.js"></script>
<script src="./js/holder.js"></script>
</body>
</html>